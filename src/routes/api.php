<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\FileController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', [AuthController::class, 'login'])->name('login');
Route::post('register', [AuthController::class, 'register'])->name('register');

Route::group(['middleware' => 'auth.jwt'], function () {
	Route::get('/', [FileController::class, 'index']);

	Route::get('folder/{id?}', [FileController::class, 'folder'])->name('get.folders');
	Route::post('folder', [FileController::class, 'storeFolder'])->name('store.folder');
	Route::delete('folder/{id?}', [FileController::class, 'destroyFile'])->name('delete.folder');

	Route::get('document/{id?}', [FileController::class, 'document'])->name('get.documents');
	Route::post('document', [FileController::class, 'storeDocument'])->name('store.document');
	Route::delete('document/{id?}', [FileController::class, 'destroyFile'])->name('delete.document');

	Route::post('refresh', [AuthController::class, 'refreshToken']);
	Route::post('logout', [AuthController::class, 'logout']);
});