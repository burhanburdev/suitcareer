<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'files';
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    protected $fillable = [
        'name',
        'type',
        'is_public',
        'owner_id',
        'timestamp',
        'company_id',
        'folder_id',
    ];
    
    public $incrementing = false;

    public function share()
    {
        return $this->hasMany(Share::class, 'file_id');
    }

    public function owner()
    {
        return $this->belongsTo(User::class, 'owner_id');
    }

    public function file()
    {
        return $this->belongsTo(File::class, 'folder_id');
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }
}
