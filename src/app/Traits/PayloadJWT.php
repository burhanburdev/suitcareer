<?php

namespace App\Traits;

use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

trait PayloadJWT
{
	public function userId()
	{
		$returnValue = null;		
		$token = JWTAuth::getToken();

		if ($token) {			
	        $payloads = JWTAuth::getPayload($token)->toArray();

	        $returnValue = $payloads['user_id'];
		}

        return $returnValue;
	}

	public function companyId()
	{
		$returnValue = null;		
		$token = JWTAuth::getToken();

		if ($token) {			
	        $payloads = JWTAuth::getPayload($token)->toArray();

	        $returnValue = $payloads['company_id'];
		}

        return $returnValue;
	}
}