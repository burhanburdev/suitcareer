<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Services\FileService;

use Exception;

class FileController extends Controller
{
    private $fileService;

	function __construct(FileService $fileService)
	{
		$this->fileService = $fileService;
	}

    public function index()
    {
    	$returnValue = [];

    	try {
    		$data = $this->fileService->getAll();

    		$code = 200;
    		$returnValue = [
    			'error' => false,
    			'data' => $data
    		];
    	} catch (Exception $ex) {
    		$code = 500;
    		$returnValue = [
	        	'error' => true, 
	        	'message' => 'Something went wrong'
	        ];
    	}

        return response()->json($returnValue, $code);
    }

    public function folder($id)
    {
    	$returnValue = [];

    	try {
    		$data = $this->fileService->getFolders($id);

    		$code = 200;
    		$returnValue = [
    			'error' => false,
    			'data' => $data
    		];
    	} catch (Exception $ex) {
    		$code = 500;
    		$returnValue = [
	        	'error' => true, 
	        	'message' => 'Something went wrong'
	        ];
    	}

        return response()->json($returnValue, $code);
    }

    public function document($id)
    {
    	$returnValue = [];

    	try {
    		$data = $this->fileService->getDocument($id);

    		$code = 200;
    		$returnValue = [
    			'error' => false,
    			'data' => $data
    		];
    	} catch (Exception $ex) {
    		$code = 500;
    		$returnValue = [
	        	'error' => true, 
	        	'message' => 'Something went wrong'
	        ];
    	}

        return response()->json($returnValue, $code);
    }

    public function storeFolder(Request $request)
    {
    	$returnValue = [];

    	try {
    		$data = $this->fileService->saveFolder($request);

    		$code = 200;
    		$returnValue = [
    			'error' => false,
    			'message' => 'folder created',
    			'data' => $data
    		];
    	} catch (Exception $ex) {
    		$code = 500;
    		$returnValue = [
	        	'error' => true, 
	        	'message' => 'Something went wrong'
	        ];
    	}

        return response()->json($returnValue, $code);
    }

    public function storeDocument(Request $request)
    {
    	$returnValue = [];

    	try {
    		$data = $this->fileService->saveDocument($request);

    		$code = 200;
    		$returnValue = [
    			'error' => false,
    			'message' => 'Success set document',
    			'data' => $data
    		];
    	} catch (Exception $ex) {
    		$code = 500;
    		$returnValue = [
	        	'error' => true, 
	        	'message' => 'Something went wrong'
	        ];
    	}

        return response()->json($returnValue, $code);
    }

    public function destroyFile(Request $request)
    {
    	$returnValue = [];

    	try {
    		$message = $this->fileService->deleteFile($request->id);

    		$code = 200;
    		$returnValue = [
    			'error' => false,
    			'message' => $message
    		];
    	} catch (Exception $ex) {
    		$code = 500;
    		$returnValue = [
	        	'error' => true, 
	        	'message' => 'Something went wrong'
	        ];
    	}

        return response()->json($returnValue, $code);
    }
}
