<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Facades\JWTAuth;

use App\Traits\PayloadJWT;

use App\Models\File;
use App\Models\Share;

use Exception;

class FileService
{
	use PayloadJWT;

	public function getAll()
	{
		$returnValue = [];

		$data = File::all();

		$i = 0;
		foreach ($data as $value) {
			$share = Share::where('file_id', $value->id)->pluck('user_id')->toArray();

			if ($value->is_public) {				
				$share = Share::where('file_id', $value->id)
				->whereHas('file', function($query) {
					$query->where('company_id', $this->companyId());
				})
				->pluck('user_id')
				->toArray();
			
				if ($value->company_id == $this->companyId()) {					
					$returnValue[$i]['id'] = $value->id;
					$returnValue[$i]['name'] = $value->name;
					$returnValue[$i]['type'] = $value->type;
					$returnValue[$i]['is_public'] = true;
					$returnValue[$i]['owner_id'] = $value->owner_id;
					$returnValue[$i]['share'] = $share;
					$returnValue[$i]['timestamp'] = $value->timestamp;
					$returnValue[$i]['company_id'] = $value->company_id;
					$returnValue[$i]['created_at'] = $value->created_at;
					$returnValue[$i]['updated_at'] = $value->updated_at;

					$i++;
				}
			} elseif (in_array($this->userId(), $share) || $value->owner_id == $this->userId()) {
				$returnValue[$i]['id'] = $value->id;
				$returnValue[$i]['name'] = $value->name;
				$returnValue[$i]['type'] = $value->type;
				$returnValue[$i]['is_public'] = false;
				$returnValue[$i]['owner_id'] = $value->owner_id;
				$returnValue[$i]['share'] = $share;
				$returnValue[$i]['timestamp'] = $value->timestamp;
				$returnValue[$i]['company_id'] = $value->company_id;
				$returnValue[$i]['created_at'] = $value->created_at;
				$returnValue[$i]['updated_at'] = $value->updated_at;

				$i++;
			}
		}

		return $returnValue;
	}

	public function getFolders($id)
	{
		$returnValue = [];

		$data = File::where('folder_id', $id)->get();

		$i = 0;
		foreach ($data as $value) {
			$share = Share::where('file_id', $value->id)->pluck('user_id')->toArray();

			$returnValue[$i]['id'] = $value->id;
			$returnValue[$i]['name'] = $value->name;
			$returnValue[$i]['type'] = $value->type;
			$returnValue[$i]['folder_id'] = $value->folder_id;
			$returnValue[$i]['timestamp'] = $value->timestamp;
			$returnValue[$i]['owner_id'] = $value->owner_id;
			$returnValue[$i]['share'] = $share;
			$returnValue[$i]['created_at'] = $value->created_at;
			$returnValue[$i]['updated_at'] = $value->updated_at;

			$i++;
		}

		return $returnValue;
	}

	public function saveFolder($request)
	{
		$data = null;
		DB::beginTransaction();

		try {			
			$check = File::find($request->id);

			if ($check) {
				$request->id = (string) Str::uuid();
			}

			$data = new File;
			$data->id = $request->id;
			$data->type = 'folder';
			$data->is_public = ($request->is_public) ? $request->is_public : false;
			$data->name = $request->name;
			$data->timestamp = $request->timestamp;
			$data->company_id = $this->companyId();
			$data->owner_id = $this->userId();
			$data->save();

			foreach ($request->share as $value) {			
				$share = new Share;
				$share->file_id = $data->id;
				$share->user_id = $value;
				$share->save();
			}

			DB::commit();
		} catch (Exception $ex) {
			DB::rollBack();
		}

		return $data;
	}	

	public function getDocument($id)
	{
		$returnValue = null;

		$data = File::find($id);

		if ($data) {
			$share = Share::where('file_id', $data->id)->pluck('user_id')->toArray();

			$returnValue['id'] = $data->id;
			$returnValue['name'] = $data->name;
			$returnValue['type'] = $data->type;
			$returnValue['folder_id'] = $data->folder_id;
			$returnValue['timestamp'] = $data->timestamp;
			$returnValue['owner_id'] = $data->owner_id;
			$returnValue['share'] = $share;
			$returnValue['created_at'] = $data->created_at;
			$returnValue['updated_at'] = $data->updated_at;
		}

		return $returnValue;
	}

	public function saveDocument($request)
	{
		$data = null;
		DB::beginTransaction();

		try {			
			$data = new File;
			$data->id = $request->id;
			$data->type = 'document';
			$data->is_public = ($request->is_public) ? $request->is_public : false;
			$data->name = $request->name;
			$data->timestamp = $request->timestamp;
			$data->company_id = $this->companyId();
			$data->owner_id = $this->userId();
			$data->folder_id = $request->folder_id;
			$data->save();

			foreach ($request->share as $value) {			
				$share = new Share;
				$share->file_id = $data->id;
				$share->user_id = $value;
				$share->save();
			}

			DB::commit();
		} catch (Exception $ex) {
			DB::rollBack();
		}

		return $data;
	}

	public function deleteFile($id)
	{
		$data = File::find($id);

		if ($data) {
			$data->delete();

			$message = 'Success delete '.$data->type;
		} else {
			$message = 'File not found';
		}

		return $message;
	}
}