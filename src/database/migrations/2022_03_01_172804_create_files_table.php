<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('name');
            $table->string('type');
            $table->boolean('is_public')->nullable();
            $table->unsignedBigInteger('owner_id');
            $table->bitInteger('timestamp');
            $table->unsignedBigInteger('company_id');
            $table->string('folder_id')->nullable();
            $table->timestamps();

            $table->foreign('owner_id')->references('id')->on('users')->onUpdate('cascade');
            $table->foreign('company_id')->references('id')->on('companies')->onUpdate('cascade');
            // $table->foreign('folder_id')->references('id')->on('files')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');
    }
}
