<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$data = [
    		[
    			'name' => 'Burhan Mafazi',
    			'email' => 'burhan@gmail.com',
    			'password' => 'burhan',
    			'company_id' => 1
    		],
    		[
    			'name' => 'Budi Setiadi',
    			'email' => 'budi@gmail.com',
    			'password' => 'budi',
    			'company_id' => 1
    		],
    		[
    			'name' => 'Putra Siregar',
    			'email' => 'putra@gmail.com',
    			'password' => 'putra',
    			'company_id' => 2
    		],
    		[
    			'name' => 'Zaki Alifia',
    			'email' => 'zaki@gmail.com',
    			'password' => 'zaki',
    			'company_id' => 3
    		],
    	];

    	foreach ($data as $value) {
	        $user = new User;
	        $user->name = $value['name'];
	        $user->email = $value['email'];
	        $user->password = Hash::make($value['password']);
	        $user->company_id = $value['company_id'];

	        $user->save();
    	}
    }
}
