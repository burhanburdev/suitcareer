<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Company;

class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$data = [
    		'Microsoft',
    		'Google',
    		'Amazon',
    		'Facebook',
    		'Twitter',
    	];

    	foreach ($data as $value) {
	        $user = new Company;
	        $user->company = $value;
	        $user->save();
    	}
    }
}
